<?php

require 'json.php';

$emails = getCommaSeparatedFieldFromJson($people, 'email');
$sortedPeople = setNameAndGetSortedDataFromJson($people, 'age', 'desc', 'name', 'first_name', 'last_name');

function decodeJson($json)
{
	$json = str_replace('},]' , '}]', $json);
	return json_decode($json, true);
}

function getCommaSeparatedFieldFromJson($json, $field)
{
	$array = decodeJson($json);
	$emails = array_column(array_shift($array), $field);
	return implode(',', $emails);
}

function setNameAndGetSortedDataFromJson($json, $sortField, $sortOrder, $newField, $field1, $field2)
{
	$array = decodeJson($json);
	$key = key($array);
	$data = array_shift($array);
	sortArrayBy($sortField, $data, $sortOrder);
	return [$key => createNewFieldFromCurrentFields($data, $newField, $field1, $field2)];
}

function sortArrayBy($field, &$array, $direction = 'asc')
{
    usort($array, function($a, $b) use ($field, $direction) { 
        $a = $a[$field];
        $b = $b[$field];

        if ($a == $b)
        {
            return 0;
        }

        return ($a  . ($direction == 'desc' ? '>' : '<') . $b) ? -1 : 1;
    });

    return true;
}

function createNewFieldFromCurrentFields(array $array, $newField, $field1, $field2)
{
	return array_reduce($array, function($carry, $item) use ($newField, $field1, $field2) {
		$item[$newField] = $item[$field1] . ' ' . $item[$field2];
		$carry[] = $item;
		return $carry;
	}, []);
}